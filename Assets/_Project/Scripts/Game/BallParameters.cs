using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BallParameters", menuName = "ScriptableObjects/BallParameters", order = 1)]
public class BallParameters : ScriptableObject
{
    [SerializeField] private Material _material;
    [SerializeField] private float _speed;
    
    public Material Material => _material;
    public float Speed => _speed;

}
