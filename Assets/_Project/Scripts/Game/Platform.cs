using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private float _speed = 20f;
    [SerializeField] private BoxCollider _boxCollider;
    [SerializeField] private Rigidbody _rigidbody;

    private Transform currentGameField;
    public void Init(Transform gameField)
    {
        currentGameField = gameField;
    }

    public void SetInputVector(Vector3 dirInput)
    {
        Vector3 velocityVector3 = (dirInput.normalized * _speed * Time.deltaTime);
        _rigidbody.position += velocityVector3;
        _rigidbody.position = ClampInGameField(_rigidbody.position, currentGameField);
    }

    public Vector3 ClampInGameField(Vector3 currentPos,Transform gameField)
    {
        float rightFieldX = gameField.position.x + (gameField.lossyScale.x/2f);
        float leftFieldX = gameField.position.x - (gameField.lossyScale.x / 2f);
        float halfX = (transform.lossyScale.x / 2f);   
        
        
        float leftMin = leftFieldX + halfX;
        float rightMax = rightFieldX - halfX;
        Vector3 clampPos  = new Vector3(Mathf.Clamp(currentPos.x,leftMin,rightMax),currentPos.y,currentPos.z);

        return clampPos;
    }
}