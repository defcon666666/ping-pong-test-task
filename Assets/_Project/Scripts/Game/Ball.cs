using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private MeshRenderer[] _meshRenderers;
    
    private Platform pelviusHitPlatform = new Platform();
    private Vector3 physicVelocity;
    private float speed;
    private bool isMove = false;
    private bool isDead = false;

    public void Init(BallParameters parameters)
    {
        physicVelocity = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized * parameters.Speed;
        speed = parameters.Speed;
        foreach (var meshRenderer in _meshRenderers)
        {
            meshRenderer.material = parameters.Material;
        }
    }

    public void StartMove()
    {
        isMove = true;
        isDead = false;
        _particleSystem.Play(true);
        transform.DOShakeScale(0.3f, Vector3.one,1, 0.3f).OnStepComplete(() =>
        {
            transform.localScale = Vector3.one;
        });
    }

    public void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        if (!isMove)
            return;

        _rigidbody.velocity = physicVelocity;
        
    }

    private void Fall()
    {
        if(isDead)
            return;
        
        isMove = false;
        isDead = true;
        physicVelocity = Vector3.zero;
        _rigidbody.velocity = Vector3.zero;
        
        _particleSystem.Stop(true);
        transform.DOScale(Vector3.zero, 0.3f).OnStepComplete(() =>
        {
            LevelManager.instance.OnLose();
            Destroy(gameObject);
        });
    }

    private void HitWithPlatform()
    {
        LevelManager.instance.OnWin();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bounce"))
        {
            Vector3 normalPlane = other.contacts[0].normal;
            Vector3 reflect = Vector3.Reflect(physicVelocity, normalPlane);
            physicVelocity = new Vector3(reflect.x,0,reflect.z).normalized *speed;
            
            Debug.DrawRay(Vector3.zero, normalPlane,Color.cyan,12f);
            Debug.DrawRay(Vector3.zero, physicVelocity.normalized,Color.red,12f);
            Debug.DrawRay(Vector3.zero, reflect,Color.yellow,12f);

            transform.DOShakeScale(0.3f, 0.3f);

            if (other.gameObject.GetComponent<Platform>() != null)
            {
                Platform hitPlatform = other.gameObject.GetComponent<Platform>();
                
                if (!pelviusHitPlatform.Equals(hitPlatform))
                {
                    HitWithPlatform();
                }

                pelviusHitPlatform = hitPlatform;
            }
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("GameOver"))
        {
            
            Fall();
        }
    }
    
}