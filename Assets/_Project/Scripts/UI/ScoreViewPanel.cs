using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class ScoreViewPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textMeshPro;
    [SerializeField] private string addedPrefix;

    public void OnEnable()
    {
        GlobalEventManager.OnChangeScope += Show;
    }

    public void OnDestroy()
    {
        GlobalEventManager.OnChangeScope -= Show;
    }

    private void Show(int value)
    {
        _textMeshPro.text = addedPrefix + value.ToString();
        _textMeshPro.transform.DOShakeScale(0.2f, 0.1f);
    }
}
