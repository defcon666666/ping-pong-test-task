using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager: MonoBehaviour 
{
    [SerializeField] private BaseManager[] _baseManagers;

    private void Awake()
    {
        foreach (var baseManager in _baseManagers)
        {
            baseManager.Init();
        }
    }
    
    private void Start()
    {
        foreach (var baseManager in _baseManagers)
        {
            baseManager.StartLevel();
        }
    }

}
