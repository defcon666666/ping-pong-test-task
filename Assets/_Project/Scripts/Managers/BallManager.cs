using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : BaseManager
{
    public static BallManager instance;
    
    [SerializeField] private Ball _ballPrefab;
    [SerializeField] private Ball _currentBall;
    [SerializeField] private BallParameters[] _ballParameters;
    [SerializeField] private Transform _spawnTransform;

    public override void Init()
    {
        instance = this;
    }

    public override void StartLevel()
    {
        _currentBall = CreateBall();
        _currentBall.StartMove();
    }

    private Ball CreateBall()
    {
        Ball ball = Instantiate(_ballPrefab, _spawnTransform.position, _spawnTransform.rotation);
        ball.transform.parent = transform;
        ball.Init(_ballParameters[Random.Range(0, _ballParameters.Length)]);
        return ball;
    }
}