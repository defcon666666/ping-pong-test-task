using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : BaseManager
{
    public static LevelManager instance;
    
    private const string bestScoreKey = "BestScore";
    
    private int countSeccionScore = 0;

    public override void Init()
    {
        instance = this;
    }

    public override void StartLevel()
    {
        ViewBest();
    }

    public void OnLose()
    {
        int best= LoadBestScope();
        if (best < countSeccionScore)
        {
            PlayerPrefs.SetInt(bestScoreKey,countSeccionScore);
        }
        countSeccionScore = 0;
        
        ViewBest();
        BallManager.instance.StartLevel();
    }

    public void OnWin()
    {
        countSeccionScore++;
    }

    private int LoadBestScope()
    {
        return PlayerPrefs.GetInt(bestScoreKey, 0);
    }

    private void ViewBest()
    {
        int viewBest = LoadBestScope();
        GlobalEventManager.SendChangePlayerScope(viewBest);
    }
}