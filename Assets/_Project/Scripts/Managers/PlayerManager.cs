using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : BaseManager
{
    [SerializeField] private DynamicJoystick _joystick;
    [SerializeField] private Platform[] _platforms;
    [SerializeField] private GameObject _boundsField;
    
    private bool isControlling = false;

    public override void Init()
    {
        foreach (var platform in _platforms)
        {
            platform.Init(_boundsField.transform);
        }
    }

    public override void StartLevel()
    {
        isControlling = true;
    }

    public void FixedUpdate()
    {
        ProcessingPlatform();
    }

    private void ProcessingPlatform()
    {
        if(!isControlling)
            return;

        foreach (var platform in _platforms)
        {
            platform.SetInputVector(_joystick.Direction);
        }
    }


}
