using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalEventManager 
{
    public static event Action<int> OnChangeScope;
    
    public static void SendChangePlayerScope(int value) => OnChangeScope?.Invoke(value);
}
