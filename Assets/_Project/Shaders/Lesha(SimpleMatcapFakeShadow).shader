// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/Lesha(SimpleMatcapFakeShadow)"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_ShadingColor("ShadingColor", Color) = (0,0,0,1)
		_MainTex("MainTex", 2D) = "white" {}
		_Emission("Emission", Range( 0 , 1)) = 0
		_Matcap_Intensity("Matcap_Intensity", Range( 0 , 1)) = 0
		_Matcap("Matcap", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

		_ShadowFloorY("_ShadowFloorY", Float) = 0
		//_ShadowColor("ShadowColor", Color) = (0.4466002,0.4725443,0.490566,1)
		_ShadowDirZ("_ShadowDirZ", Range( -1 , 1)) = 0
		_ShadowDirX("_ShadowDirX", Range( -1 , 1)) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Opaque" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend Off
		AlphaToMask Off
		Cull Back
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset 0 , 0
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_WORLD_POSITION


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				float3 ase_normal : NORMAL;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
				#endif
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			//This is a late directive
			
			uniform float4 _Color;
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float _Emission;
			uniform float4 _ShadingColor;
			uniform float _Matcap_Intensity;
			uniform sampler2D _Matcap;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float3 ase_worldNormal = UnityObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord2.xyz = ase_worldNormal;
				
				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				o.ase_texcoord2.w = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = vertexValue;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
				#endif
				float2 uv_MainTex = i.ase_texcoord1.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 temp_output_75_0 = ( _Color * tex2D( _MainTex, uv_MainTex ) );
				float3 worldSpaceLightDir = UnityWorldSpaceLightDir(WorldPosition);
				float3 ase_worldNormal = i.ase_texcoord2.xyz;
				float dotResult69 = dot( worldSpaceLightDir , ase_worldNormal );
				float temp_output_70_0 = (dotResult69*0.5 + 0.5);
				float4 temp_cast_0 = (temp_output_70_0).xxxx;
				float4 lerpResult76 = lerp( temp_cast_0 , _ShadingColor , ( 1.0 - temp_output_70_0 ));
				float4 tex2DNode51 = tex2D( _Matcap, ((mul( unity_WorldToCamera, float4( ase_worldNormal , 0.0 ) ).xyz).xy*0.5 + 0.5) );
				
				
				finalColor = ( ( temp_output_75_0 * _Emission ) + ( temp_output_75_0 * lerpResult76 ) + ( ( ( 1.0 - _Emission ) * _Matcap_Intensity ) * tex2DNode51 ) );
				return finalColor;
			}
			ENDCG
		}
			/////////////////Shadow////////////////

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		Stencil
		{
			Ref 1
			Comp Equal
			Pass Keep
			Fail Keep
			ZFail Keep
		}

		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha noshadow vertex:vertexDataFunc

		struct Input
		{
			half filler;
		};

		uniform float _ShadowFloorY;
		uniform float _ShadowDirX;
		uniform float _ShadowDirZ;
		uniform float4 _ShadowColor;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 transform29 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float4 transform26 = mul(unity_ObjectToWorld,float4( 0,0,0,1 ));
			float temp_output_30_0 = ( _ShadowFloorY - transform26.y );
			float temp_output_33_0 = ( transform29.y - temp_output_30_0 );
			float4 break35 = transform29;
			float3 appendResult39 = (float3(( ( temp_output_33_0 * _ShadowDirX ) + break35.x ) , temp_output_30_0 , ( ( temp_output_33_0 * _ShadowDirZ ) + break35.z )));
			float4 lerpResult43 = lerp( float4( appendResult39 , 0.0 ) , transform29 , step( ( transform29.y + transform26.y ) , _ShadowFloorY ));
			float4 transform40 = mul(unity_WorldToObject,lerpResult43);
			v.vertex.xyz = transform40.xyz;
			v.vertex.w = 1;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 temp_output_25_0 = _ShadowColor;
			o.Emission = temp_output_25_0.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
